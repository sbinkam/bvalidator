/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



var bRuleObj = {
    /**
     * This function validates an IPAddress
     */
    ipAddress:function(o){
        for(var i=0;i<o.length;i++){
            var m = o[i].value.match("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$");
            if(m!=null){
                o[i].setAttribute("style","border:1px solid green");
            }else{
                o[i].setAttribute("style","border:1px solid red");
            }
            
        }
    },
    /**
     * This function validates an email
     */
    email:function(o){
        for(var i=0;i<o.length;i++){
            var m = o[i].value.match("^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})");
            if(m!=null){
                o[i].setAttribute("style","border:1px solid green");
            }else{
                o[i].setAttribute("style","border:1px solid red");
            }
            
        }
    },
    /**
     * This function validates an integer
     */
    numberValidation:function(o){
        for(var i=0;i<o.length;i++){
           var m = o[i].value.match("^([0-9]*)([0-9]+)$");
            if(m!=null){
                o[i].setAttribute("style","border:1px solid green");
            }else{
                o[i].setAttribute("style","border:1px solid red");
            }
        }
    },
    /**
     * This function validates an signed number
     */
    signedValidation:function(o){
        for(var i=0;i<o.length;i++){
           var m = o[i].value.match("^(-?[0-9]*)([0-9]+)$");
            if(m!=null){
                o[i].setAttribute("style","border:1px solid green");
            }else{
                o[i].setAttribute("style","border:1px solid red");
            }
        }
    },
    /**
     * This function validates a decimal
     */
    decimalValidation:function(o){
        for(var i=0;i<o.length;i++){
           var m = o[i].value.match("^([0-9]*)([.])([0-9]+)$");
            if(m!=null){
                o[i].setAttribute("style","border:1px solid green");
            }else{
                o[i].setAttribute("style","border:1px solid red");
            }
        }
    },
    /**
     * This function is the trigger. You need to call 
     * this function to apply rules
     */
    applyRule:function(){
        this.email($(".email"));
        this.ipAddress($(".ipaddress"));
        this.numberValidation($(".number"));
        this.signedValidation($(".signed"));
        this.decimalValidation($(".decimal"));
        
    }
};
